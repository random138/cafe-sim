# Sim Game

Just a little cafe/dating sim game.

Plan is to have basic relationship dynamics and
resource management.

Stretch goal is to have an external political environment
you can affect by lobbying and advertising, which makes you
choose between trust from the working class and minimizing
taxes and wages.

Oh and it's a cat cafe but not like the normal one like a
cafe where all the workers are femboys with cat ears and tails.

# Version 1

- (DONE) very bad developer art
- (DONE) some buttons that take you between scenes
- keeping personal information of each cat and how they
  relate to you and eachother
- kitchen inventory management and meals simulation
- simulate preparing orders for customers and inventory management

